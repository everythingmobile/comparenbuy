package com.springapp.mvc.Controller;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.ScrapingService;
import com.springapp.mvc.Service.ServiceFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/24/13
 * Time: 12:10 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TestController {

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test() {
        ScrapingService flipkartScrapingService = ServiceFactory.getFlipkartScrapingService();
        return flipkartScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
    }

    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test1() {
        ScrapingService amazonScrapingService = ServiceFactory.getAmazonScrapingService();
        return amazonScrapingService.getProductsForPage(new Query("Iphone 5c", "1"));
    }

    @RequestMapping(value = "/test2", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test2() {
        ScrapingService snapdealScrapingService = ServiceFactory.getSnapdealScrapingService();
        return snapdealScrapingService.getProductsForPage(new Query("htc one", "1"));
    }

    @RequestMapping(value = "/test3", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test3() {
        ScrapingService tradusScrapingService = ServiceFactory.getTradusScrapingService();
        return tradusScrapingService.getProductsForPage(new Query("micromax canvas hd", "1"));
    }

    @RequestMapping(value = "/test4", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test4() {
        ScrapingService infibeamScrapingService = ServiceFactory.getInfibeamScrapingService();
        return infibeamScrapingService.getProductsForPage(new Query("mp3 player", "1"));
    }

    @RequestMapping(value = "/test5", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test5() {
        ScrapingService ebayScrapingService = ServiceFactory.getEbayScrapingService();
        return ebayScrapingService.getProductsForPage(new Query("micromax canvas hd", "1"));
    }

    @RequestMapping(value = "/test6", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test6() {
        ScrapingService naaptolScrapingService = ServiceFactory.getNaaptolScrapingService();
        return naaptolScrapingService.getProductsForPage(new Query("nokia", "2"));
    }

    @RequestMapping(value = "/test7", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> test7() {
        ScrapingService homeScrapingService = ServiceFactory.geHomeShop18ScrapingService();
        return homeScrapingService.getProductsForPage(new Query("iphone", "1"));
    }
}
