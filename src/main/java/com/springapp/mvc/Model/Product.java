package com.springapp.mvc.Model;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 6:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class Product
{
	private String name;
	private String price;
	private String imageUrl;
	private String productLink;

	public Product(String name, String price, String imageUrl, String productLink)
	{
		this.name = name;
		this.price = price;
		this.imageUrl = imageUrl;
		this.productLink = productLink;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPrice(String price)
	{
		this.price = price;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public void setProductLink(String productLink)
	{
		this.productLink = productLink;
	}

	public String getName()
	{

		return name;
	}

	public String getPrice()
	{
		return price;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public String getProductLink()
	{
		return productLink;
	}

	public String toString()
	{
		return getName() + "|" + getPrice() + "|" + getProductLink() + "|" + getImageUrl();
	}
}
