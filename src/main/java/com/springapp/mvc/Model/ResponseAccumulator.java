package com.springapp.mvc.Model;

import com.springapp.mvc.Service.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 1/31/14.
 */
public class ResponseAccumulator {
    private List<SiteResponse> responses;
    private static ResponseAccumulator responseAccumulator;

    private ResponseAccumulator() {
    }

    public static ResponseAccumulator getResponseAccumulator() {
        if (responseAccumulator == null) {
            responseAccumulator = new ResponseAccumulator();
        }
        return responseAccumulator;
    }

    public List<SiteResponse> getSiteResponses(Query query) {
        this.responses = new ArrayList<SiteResponse>();
        this.responses.add(new SiteResponse("AmazonResponse", ServiceFactory.getAmazonScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("EbayResponse", ServiceFactory.getEbayScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("FlipkartResponse", ServiceFactory.getFlipkartScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("InfibeamResponse", ServiceFactory.getInfibeamScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("NaaptolResponse", ServiceFactory.getNaaptolScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("SnapdealResponse", ServiceFactory.getSnapdealScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("TradusResponse", ServiceFactory.getTradusScrapingService().getProductsForPage(query)));
        this.responses.add(new SiteResponse("HomeShop18Response", ServiceFactory.geHomeShop18ScrapingService().getProductsForPage(query)));
        return this.responses;
    }
}
