package com.springapp.mvc.Model;

import com.springapp.mvc.Service.ScrapingService;

import java.util.List;

/**
 * Created by vivek on 1/31/14.
 */
public class SiteResponse
{
    private String siteName;
    private List<Product> products;

    public SiteResponse(String siteName, List<Product> products) {
        this.siteName = siteName;
        this.products = products;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
