package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class FlipkartScrapingService extends ScrapingService {
    private Properties properties = new Properties();

    private String getProductTitleFromElement(Element element) {
        return element.select(properties.getProperty("flipkart.product.displayBlock")).first().attr("title");
    }

    private String getProductImageUrlFromElement(Element element) {
        String imgUrl = element.select("img").first().attr(properties.getProperty("flipkart.product.imgAttrName"));
        if (imgUrl == "")
            imgUrl = element.select("img").first().attr(properties.getProperty("flipkart.product.imgAttrNameIfNot"));
        return imgUrl;
    }

    private String getProductLinkFromElement(Element element) {
        return "http://www.flipkart.com" + element.select(properties.getProperty("flipkart.product.displayBlock")).first().attr("href");
    }

    private String getProductPriceFromElement(Element element) {
        return element.select(properties.getProperty("flipkart.product.prodPriceAttrName")).text();
    }

    private void loadProperties() {
        try {
            properties.load(FlipkartScrapingService.class.getClassLoader().getResourceAsStream("flipkart.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.flipkart.com/search?q=";
        url += query.getProductName();
        url += "&start=";
        int page = Integer.parseInt(query.getPageNumber());
        page = 1 + (page - 1) * 20;
        url += page;
        List<Product> productList = new ArrayList<Product>();

        loadProperties();

        try {
            Document document = Jsoup.connect(url).get();
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("flipkart.fetchNumber"));
            Elements elements = document.select(properties.getProperty("flipkart.product.holderDiv"));
            int count = 0;
            for (Element element : elements) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );

                productList.add(product);

                //logger.log(s+" at price "+price+" image url "+img+" link to product "+prodLink);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return productList;
    }
}
