package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class InfibeamScrapingService extends ScrapingService {
    private Properties properties = new Properties();

    private void loadProperties() {

        try {
            properties.load(InfibeamScrapingService.class.getClassLoader().getResourceAsStream("infibeam.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getProductTitleFromElement(Element element) {
        String title = element.getElementsByClass(properties.getProperty("infibeam.product.title")).first().text();
        return title;
    }

    private String getProductPriceFromElement(Element element) {
        String price = "";
        Element priceElement = element.select(properties.getProperty("infibeam.product.priceDiv")).first();
        if (priceElement.select(properties.getProperty("infibeam.product.priceSpan")).first() != null) {
            price = priceElement.select(properties.getProperty("infibeam.product.priceSpan")).first().text();
        } else {
            price = priceElement.select(properties.getProperty("infibeam.product.priceAlt")).first().text();
        }

        return price;
    }

    private String getProductImageUrlFromElement(Element element) {
        String img = element.getElementsByTag(properties.getProperty("infibeam.product.image")).first().attr(properties.getProperty("infibeam.product.imageAttr"));
        return img;
    }

    private String getProductLinkFromElement(Element element) {
        String link = element.select(properties.getProperty("infibeam.product.link")).first().attr(properties.getProperty("infibeam.product.linkAttr"));
        return "http://www.infibeam.com" + link;
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String url = "http://www.infibeam.com/search?q=";
        // url+=productName;
        url += URLEncoder.encode(query.getProductName());
        url += "&page=" + query.getPageNumber();
        List<Product> productList = new ArrayList<Product>();
        loadProperties();
        try {
            Document document = Jsoup.connect(url).userAgent("Chrome").get();
            Elements elements = document.select(properties.getProperty("infibeam.product.holderUl"));
            Elements elements1 = new Elements();
            for (Element e : elements) {
                elements1.addAll(e.select(properties.getProperty("infibeam.product.holderLi")));
            }
            int count = 0;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("infibeam.fetchNumber"));
            for (Element element : elements1) {
                if (count == maximumProductsToFetch) break;
                count++;

                Product product = new Product(
                        getProductTitleFromElement(element), getProductPriceFromElement(element), getProductImageUrlFromElement(element), getProductLinkFromElement(element)
                );
                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }
}
