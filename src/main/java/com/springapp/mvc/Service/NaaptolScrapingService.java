package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;
import com.springapp.mvc.Service.FlipkartScrapingService;
import com.springapp.mvc.Service.ScrapingService;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;


public class NaaptolScrapingService extends ScrapingService {
    private Properties properties = new Properties();

    private void loadProperties() {

        try {
            properties.load(NaaptolScrapingService.class.getClassLoader().getResourceAsStream("naaptol.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Product> runSiteRoutine(Query query) {
        String urlString = "http://www.naaptol.com/faces/jsp/ajax/ajax.jsp?type=srch_catlg&kw=";
        urlString += query.getProductName();
        int page = Integer.parseInt(query.getPageNumber());
        urlString += "&pi=" + page + "&requesttype=ajaxNextPage&actionname=getCatlogNextPageData";
        List<Product> productList = new ArrayList<Product>();
        loadProperties();

        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection connection = null;
            if (urlConnection instanceof HttpURLConnection) {
                connection = (HttpURLConnection) urlConnection;
            }
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String responseString = "";
            String current;
            while ((current = in.readLine()) != null) {
                responseString += current;
            }
            ObjectMapper mapper = new ObjectMapper();

            List<LinkedHashMap<String, Object>> jsonMap = mapper.readValue(responseString, List.class);
            List<LinkedHashMap<String, Object>> products = jsonMap;
            if (products == null) return productList;
            int maximumProductsToFetch = Integer.parseInt(properties.getProperty("naaptol.fetchNumber"));
            int count = 0;
            for (LinkedHashMap<String, Object> item : products) {
                if (count == maximumProductsToFetch) break;
                count++;
                String name = item.get("productName") != null ? item.get("productName").toString() : "";
                String price = item.get("productPrice") != null ? item.get("productPrice").toString() : "";
                String image = item.get("imageName") != null ? "images.naaptol.com/usr/local/csp/staticContent/NormImg105x105/" + item.get("imageName") : "";
                String staticUrl = item.get("staticUrl") != null ? item.get("staticUrl").toString().toLowerCase() : "";
                staticUrl = staticUrl.replace(' ', '-');
                String categoryStaticUrl = item.get("categoryStaticUrl") != null ? item.get("categoryStaticUrl").toString().toLowerCase() : "";
                categoryStaticUrl = categoryStaticUrl.replace(' ', '-');
                String productId = item.get("productId") != null ? item.get("productId").toString() : "";
                String productLink = "http://www.naaptol.com/" + categoryStaticUrl + "/" + staticUrl + "/p/" + productId + ".html";
                Product product = new Product(name, price, image, productLink);
                productList.add(product);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {

            e.printStackTrace();

        } catch (JsonMappingException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return productList;
    }
}
