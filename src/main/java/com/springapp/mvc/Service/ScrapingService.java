package com.springapp.mvc.Service;

import com.springapp.mvc.Model.Product;
import com.springapp.mvc.Model.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: vivek
 * Date: 11/23/13
 * Time: 6:12 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ScrapingService {
    Map<Query, List<Product>> productsCache = new HashMap<Query, List<Product>>();

    public abstract List<Product> runSiteRoutine(Query query);

    public List<Product> getProductsForPage(Query query) {

        if (productsCache.containsKey(query)) {
            return productsCache.get(query);
        }

        List<Product> products = runSiteRoutine(query);
        productsCache.put(query, products);

        return products;
    }

}
